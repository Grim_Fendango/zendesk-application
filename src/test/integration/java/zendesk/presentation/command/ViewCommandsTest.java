package zendesk.presentation.command;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.shell.Bootstrap;
import org.springframework.shell.core.CommandResult;
import org.springframework.shell.core.JLineShellComponent;

public class ViewCommandsTest {

    @Test
    public void givenValidConfigViewFieldsForAllEntitiesCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("view fields");

        Assertions.assertThat(cr.isSuccess()).isTrue();
        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void givenInvalidEntityTypeViewFieldsCommandShouldFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("view fields ork");

        Assertions.assertThat(cr.getResult()).isEqualTo(false);
    }

    @Test
    public void organizationViewFieldsCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("view fields organization");

        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void userViewFieldsCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("view fields user");

        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void ticketViewFieldsCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("view fields ticket");

        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }
}
