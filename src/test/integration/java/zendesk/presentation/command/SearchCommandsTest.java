package zendesk.presentation.command;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.shell.Bootstrap;
import org.springframework.shell.core.CommandResult;
import org.springframework.shell.core.JLineShellComponent;

public class SearchCommandsTest {

    @Test
    public void organizationSearchCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search organization --field shared_tickets --value false");

        Assertions.assertThat(cr.isSuccess()).isTrue();
        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void userSearchCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search user --field role --value admin");

        Assertions.assertThat(cr.isSuccess()).isTrue();
        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void ticketSearchCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search ticket --field has_incidents --value false");

        Assertions.assertThat(cr.isSuccess()).isTrue();
        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void givenValidParamsAndConfigSearchCommandShouldNotFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search user --field _id --value 1");

        Assertions.assertThat(cr.isSuccess()).isTrue();
        Assertions.assertThat(cr.getResult()).isEqualTo(true);
    }

    @Test
    public void givenInvalidEntityTypeSearchCommandShouldFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search fred --field _id --value 1");

        Assertions.assertThat(cr.getResult()).isEqualTo(false);
    }

    @Test
    public void givenInvalidFieldParamSearchCommandShouldFail() {
        Bootstrap bootstrap = new Bootstrap();
        JLineShellComponent shell = bootstrap.getJLineShellComponent();

        CommandResult cr = shell.executeCommand("search user --field fred --value 1");

        Assertions.assertThat(cr.getResult()).isEqualTo(false);
    }
}
