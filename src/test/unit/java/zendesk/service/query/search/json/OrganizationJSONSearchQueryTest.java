package zendesk.service.query.search.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import zendesk.models.data.Organization;
import zendesk.models.data.Ticket;
import zendesk.models.data.User;
import zendesk.models.service.query.search.SearchQueryResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.presentation.render.search.shell.ShellOrganizationSearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrganizationJSONSearchQueryTest {

    private ApplicationConfiguration configuration = mock(ApplicationConfiguration.class);
    private SearchQueryResultsRenderer renderer = mock(ShellOrganizationSearchQueryResultsRenderer.class);
    private SearchQueryJSONArrayCursor organizationJSONArrayCursor = mock(SearchQueryJSONArrayCursor.class);
    private SearchQueryJSONArrayCursor userJSONArrayCursor = mock(SearchQueryJSONArrayCursor.class);
    private SearchQueryJSONArrayCursor ticketJSONArrayCursor = mock(SearchQueryJSONArrayCursor.class);
    private JsonNode organizationJsonNode;

    private static final ObjectMapper mapper = new ObjectMapper();


    @Before
    public void setUp() throws Exception {
        when(configuration.isValid()).thenReturn(true);
        when(configuration.getOrganizationDataPath()).thenReturn("/");
        when(configuration.getUserDataPath()).thenReturn("/");
        when(configuration.getTicketDataPath()).thenReturn("/");

        when(renderer.resultsStart()).thenReturn(true);
        when(renderer.enterResult(any(SearchQueryResultEntry.class))).thenReturn(true);
        when(renderer.resultsEnd()).thenReturn(true);

        when(organizationJSONArrayCursor.start()).thenReturn(true);
        doNothing().when(organizationJSONArrayCursor).close();
        when(userJSONArrayCursor.start()).thenReturn(true);
        doNothing().when(userJSONArrayCursor).close();
        when(ticketJSONArrayCursor.start()).thenReturn(true);
        doNothing().when(ticketJSONArrayCursor).close();
    }

    @Test
    public void shouldRenderEachResult() throws Exception {
        when(userJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(ticketJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(organizationJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class)))
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(false); // 3 results
        when(organizationJSONArrayCursor.getCurrentEntityNode()).thenReturn(getOrganizationJsonNode());

        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration,
                renderer, organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);
        query.run();
        verify(renderer, atLeast(3)).enterResult(any(SearchQueryResultEntry.class));
    }

    @Test
    public void shouldNotFail() throws Exception {
        when(organizationJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class)))
                .thenReturn(true)
                .thenReturn(false);
        when(userJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class)))
                .thenReturn(true)
                .thenReturn(false);
        when(ticketJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class)))
                .thenReturn(true)
                .thenReturn(false);
        when(organizationJSONArrayCursor.getCurrentEntityNode()).thenReturn(getOrganizationJsonNode());
        when(userJSONArrayCursor.getCurrentEntityNode()).thenReturn(getUserJsonNode());
        when(ticketJSONArrayCursor.getCurrentEntityNode()).thenReturn(getTicketJsonNode());

        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration,
                renderer, organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);

        Assertions.assertThat(query.run()).isTrue();
    }

    @Test
    public void shouldFailWhenInvalidConfig() throws Exception {
        when(configuration.isValid()).thenReturn(false);
        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration,
                renderer, organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);
        Assertions.assertThat(query.run()).isFalse();
    }

    @Test
    public void shouldFailWhenRenderStartFails() throws Exception {
        when(renderer.resultsStart()).thenReturn(false);
        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration,
                renderer, organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);
        Assertions.assertThat(query.run()).isFalse();
    }

    @Test
    public void shouldFailWhenRenderResultFails() throws Exception {
        when(renderer.resultsStart()).thenReturn(true);
        when(renderer.resultsEnd()).thenReturn(true);
        when(userJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(ticketJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(organizationJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class)))
                .thenReturn(true)
                .thenReturn(false);
        when(organizationJSONArrayCursor.getCurrentEntityNode()).thenReturn(getOrganizationJsonNode());

        when(renderer.enterResult(any(SearchQueryResultEntry.class))).thenReturn(false);
        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration, renderer,
                organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);

        Assertions.assertThat(query.run()).isFalse();
    }

    @Test
    public void shouldFailWhenRenderEndFails() throws Exception {
        when(renderer.resultsStart()).thenReturn(true);
        when(renderer.enterResult(any(SearchQueryResultEntry.class))).thenReturn(true);
        when(organizationJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(userJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);
        when(ticketJSONArrayCursor.nextMatchingNode(any(String.class), any(String.class))).thenReturn(false);

        when(renderer.resultsEnd()).thenReturn(false);
        OrganizationJSONSearchQuery query = new OrganizationJSONSearchQuery("_id", "101", configuration,
                renderer, organizationJSONArrayCursor, userJSONArrayCursor, ticketJSONArrayCursor);

        Assertions.assertThat(query.run()).isFalse();
    }

    public JsonNode getOrganizationJsonNode() {
        Organization organization = new Organization("101", "http://initech.zendesk.com/api/v2/organizations/101.json",
                "9270ed79-35eb-4a38-a46f-35725197ea8d", "Enthaze",
                ImmutableList.of("kage.com", "ecratic.com", "endipin.com", "zentix.com", "zentix.com", "zentix.com", "zentix.com"),
                "2016-05-21T11:10:28 -10:00", "MegaCorp", "false",
                ImmutableList.of("Fulton", "West", "Rodriguez"));

        return mapper.convertValue(organization, JsonNode.class);
    }


    public JsonNode getUserJsonNode() {
        Ticket ticket = new Ticket("436bf9b0-1147-4c0a-8439-6f79833bff5b",
                "http://initech.zendesk.com/api/v2/tickets/436bf9b0-1147-4c0a-8439-6f79833bff5b.json",
                "9210cdc9-4bee-485f-a078-35396cd74063", "2016-04-28T11:19:34 -10:00", "incident",
                "A Catastrophe in Korea (North)", "Nostrud ad sit velit cupidatat", "high", "pending", "38", "24",
                "116", ImmutableList.of("tag1", "tag2"), "false", "2016-07-31T02:37:50 -10:00", "web");


        return mapper.convertValue(ticket, JsonNode.class);
    }

    public JsonNode getTicketJsonNode() {
        User user = new User("1", "http://initech.zendesk.com/api/v2/users/1.json",
                "74341f74-9c79-49d5-9611-87ef9b6eb75f", "Francisca Rasmussen", "Miss Coffey",
                "2016-04-15T05:19:46 -10:00", "true", "true", "false", "en-AU", "Sri Lanka",
                "2013-08-04T01:03:27 -10:00", "coffeyrasmussen@flotonic.com", "8335-422-718", "Don't Worry Be Happy!",
                "119", ImmutableList.of("tag1", "tag2"), "true", "admin");

        return mapper.convertValue(user, JsonNode.class);
    }
}