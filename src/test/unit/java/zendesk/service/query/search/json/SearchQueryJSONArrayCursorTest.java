package zendesk.service.query.search.json;

import com.fasterxml.jackson.databind.JsonNode;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Test;

import java.nio.file.Paths;

public class SearchQueryJSONArrayCursorTest {

    // TODO: use data from test resources instead
    private static final String JSON_PATH = Paths.get("").toAbsolutePath().toString() + "/data/users.json";
    private SearchQueryJSONArrayCursor cursor = new SearchQueryJSONArrayCursor(JSON_PATH) ;

    @After
    public void tearDown() throws Exception {
        cursor.close();
    }

    @Test
    public void shouldMoveToStart() throws Exception {
        cursor.start();
        Assertions.assertThat(cursor.getCurrentEntityNode()).isNull();
    }

    @Test
    public void shouldMoveToNextMatchingNodeWhenSubsequentMatchExists() throws Exception {
        cursor.start();
        cursor.nextMatchingNode("suspended", "true");
        JsonNode firstNode = cursor.getCurrentEntityNode();
        cursor.nextMatchingNode("suspended", "true");
        JsonNode secondNode = cursor.getCurrentEntityNode();
        Assertions.assertThat(firstNode.get("_id")).isNotEqualTo(secondNode.get("_id"));
    }

    @Test
    public void shouldMoveToEndWhenNoSubsequentMatchExists() throws Exception {
        SearchQueryJSONArrayCursor cursor = new SearchQueryJSONArrayCursor(JSON_PATH);
        cursor.start();
        cursor.nextMatchingNode("suspended", "true");
        cursor.nextMatchingNode("_id", "NOT AN ID!!");
        Assertions.assertThat(cursor.getCurrentEntityNode()).isNull();
    }

    @Test
    public void getMatchesFound() throws Exception {
        cursor.start();
        cursor.nextMatchingNode("suspended", "true");
        cursor.nextMatchingNode("suspended", "true");
        cursor.nextMatchingNode("suspended", "true");
        Assertions.assertThat(cursor.getMatchesFound()).isEqualTo(3);
    }

    @Test
    public void shouldNotFindMoreThanMaxMatches() throws Exception {
        int maxResults = 2;
        SearchQueryJSONArrayCursor cursor = new SearchQueryJSONArrayCursor(JSON_PATH, maxResults);
        cursor.start();
        cursor.nextMatchingNode("suspended", "true");
        cursor.nextMatchingNode("suspended", "true");
        cursor.nextMatchingNode("suspended", "true");
        Assertions.assertThat(cursor.getMatchesFound()).isLessThanOrEqualTo(maxResults);
    }

    @Test
    public void shouldFindAllMatchesWhenNoMaximumSet() throws Exception {
        cursor.start();
        while(cursor.nextMatchingNode("role", "admin")) {}
        Assertions.assertThat(cursor.getMatchesFound()).isGreaterThan(5);
    }

}