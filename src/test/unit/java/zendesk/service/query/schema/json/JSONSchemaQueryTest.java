package zendesk.service.query.schema.json;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import zendesk.models.service.query.schema.SchemaQueryResult;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import static org.mockito.Mockito.*;

import java.util.Optional;

public class JSONSchemaQueryTest {

    private static final String ORGANIZATION = "ORGANIZATION";
    private static final String USER = "USER";
    private static final String TICKET = "TICKET";

    private static final String USER_FIELD = "role";
    private static final String ORGANIZATION_FIELD = "domain_names";
    private static final String TICKET_FIELD = "priority";

    private ApplicationConfiguration configuration = mock(ApplicationConfiguration.class);


    @Before
    public void setUp() throws Exception {
        when(configuration.getSupportedEntityTypes()).thenReturn(ImmutableList.of(ORGANIZATION, USER, TICKET));
        when(configuration.isValid()).thenReturn(true);
    }

    @Test
    public void shouldFailWithInvalidConfiguration() throws Exception {
        ApplicationConfiguration invalidConfiguration = mock(ApplicationConfiguration.class);
        when(invalidConfiguration.getSupportedEntityTypes()).thenReturn(ImmutableList.of(ORGANIZATION, USER, TICKET));
        when(invalidConfiguration.isValid()).thenReturn(false);
        JSONSchemaQuery query = new JSONSchemaQuery(null, invalidConfiguration, null);
        Assertions.assertThat(query.run()).isFalse();
    }

    @Test
    public void shouldSucceedWithValidConfiguration() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(null, configuration, null);
        Assertions.assertThat(query.run()).isTrue();
    }

    @Test
    public void shouldFindOrganizationFields() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(ORGANIZATION, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(ORGANIZATION))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(ORGANIZATION_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldFindUserFields() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(USER, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(USER))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(USER_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldFindTicketFields() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(TICKET, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(TICKET))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(TICKET_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldFindOrganizationFieldInAllEntityTypeQuery() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(null, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(ORGANIZATION))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(ORGANIZATION_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldFindUserFieldInAllEntityTypeQuery() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(null, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(USER))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(USER_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldFindTicketFieldInAllEntityTypeQuery() throws Exception {
        JSONSchemaQuery query = new JSONSchemaQuery(null, configuration, null);
        query.run();
        Optional<SchemaQueryResult> result = query.getResults().stream()
                .filter(res -> res.getEntityType().equals(TICKET))
                .findFirst();
        Assertions.assertThat(result.isPresent()).isTrue();
        Assertions.assertThat(TICKET_FIELD).isIn(result.get().getFields());
    }

    @Test
    public void shouldRenderMultipleResults() throws Exception {
        SchemaQueryResultsRenderer renderer = mock(SchemaQueryResultsRenderer.class);
        JSONSchemaQuery query = new JSONSchemaQuery(null, configuration, renderer);
        query.run();
        verify(renderer).renderResults(query.getResults());
    }

    @Test
    public void shouldRenderSingleResult() throws Exception {
        SchemaQueryResultsRenderer renderer = mock(SchemaQueryResultsRenderer.class);
        JSONSchemaQuery query = new JSONSchemaQuery(ORGANIZATION, configuration, renderer);
        query.run();
        verify(renderer).renderResult(query.getResults().get(0));
    }
}