package zendesk.service.validation.json;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import zendesk.service.configuration.ApplicationConfiguration;

import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JSONSourceDataValidatorTest {

    private ApplicationConfiguration configuration = mock(ApplicationConfiguration.class);

    // TODO: use data from test resources instead
    private static final String WORKING_DIR = Paths.get("").toAbsolutePath().toString();
    private static final String ORGANIZATION_DATA_PATH = WORKING_DIR + "/data/organizations.json";
    private static final String USER_DATA_PATH = WORKING_DIR + "/data/users.json";
    private static final String TICKET_DATA_PATH = WORKING_DIR + "/data/tickets.json";

    private static final String INVALID_ORGANIZATION_DATA_PATH = WORKING_DIR + "/data/invalid-organizations.json";
    private static final String INVALID_USER_DATA_PATH = WORKING_DIR + "/data/invalid-users.json";
    private static final String INVALID_TICKET_DATA_PATH = WORKING_DIR + "/data/invalid-tickets.json";

    @Test
    public void shouldNotFailWhenAllValidJson() throws Exception {
        when(configuration.isValid()).thenReturn(true);
        when(configuration.getOrganizationDataPath()).thenReturn(ORGANIZATION_DATA_PATH);
        when(configuration.getUserDataPath()).thenReturn(USER_DATA_PATH);
        when(configuration.getTicketDataPath()).thenReturn(TICKET_DATA_PATH);
        JSONSourceDataValidator validator = new JSONSourceDataValidator(configuration);
        Assertions.assertThat(validator.isAllEntityDataValid()).isTrue();
    }

    @Test
    public void shouldFailWhenInvalidConfiguration() throws Exception {
        when(configuration.isValid()).thenReturn(false);
        JSONSourceDataValidator validator = new JSONSourceDataValidator(configuration);
        Assertions.assertThat(validator.isAllEntityDataValid()).isEqualTo(false);
    }

    @Test
    public void shouldFailWhenInvalidOrganizationJson() throws Exception {
        when(configuration.isValid()).thenReturn(true);
        when(configuration.getOrganizationDataPath()).thenReturn(INVALID_ORGANIZATION_DATA_PATH);
        when(configuration.getUserDataPath()).thenReturn(USER_DATA_PATH);
        when(configuration.getTicketDataPath()).thenReturn(TICKET_DATA_PATH);
        JSONSourceDataValidator validator = new JSONSourceDataValidator(configuration);
        Assertions.assertThat(validator.isAllEntityDataValid()).isFalse();
    }

    @Test
    public void shouldFailWheninvalidUserJson() throws Exception {
        when(configuration.isValid()).thenReturn(true);
        when(configuration.getOrganizationDataPath()).thenReturn(ORGANIZATION_DATA_PATH);
        when(configuration.getUserDataPath()).thenReturn(INVALID_USER_DATA_PATH);
        when(configuration.getTicketDataPath()).thenReturn(TICKET_DATA_PATH);
        JSONSourceDataValidator validator = new JSONSourceDataValidator(configuration);
        Assertions.assertThat(validator.isAllEntityDataValid()).isFalse();
    }

    @Test
    public void shouldFailWheninvalidTicketJson() throws Exception {
        when(configuration.isValid()).thenReturn(true);
        when(configuration.getOrganizationDataPath()).thenReturn(ORGANIZATION_DATA_PATH);
        when(configuration.getUserDataPath()).thenReturn(USER_DATA_PATH);
        when(configuration.getTicketDataPath()).thenReturn(INVALID_TICKET_DATA_PATH);
        JSONSourceDataValidator validator = new JSONSourceDataValidator(configuration);
        Assertions.assertThat(validator.isAllEntityDataValid()).isFalse();
    }
}