package zendesk.presentation.render.schema.shell;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import zendesk.models.service.query.schema.SchemaQueryResult;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;

import java.util.List;

public class ShellSchemaQueryResultsRendererTest {
    @Test
    public void renderResultShouldNotFail() throws Exception {
        SchemaQueryResultsRenderer renderer = new ShellSchemaQueryResultsRenderer();
        SchemaQueryResult result = new SchemaQueryResult("ORGANIZATION", ImmutableList.of("_id"));
        Assertions.assertThat(renderer.renderResult(result)).isTrue();
    }

    @Test
    public void renderResults() throws Exception {
        SchemaQueryResultsRenderer renderer = new ShellSchemaQueryResultsRenderer();
        List<SchemaQueryResult> results = ImmutableList.of(
                new SchemaQueryResult("ORGANIZATION", ImmutableList.of("_id")));
        Assertions.assertThat(renderer.renderResults(results)).isTrue();
    }

    @Test
    public void renderEntityTypesListShouldNotFail() throws Exception {
        SchemaQueryResultsRenderer renderer = new ShellSchemaQueryResultsRenderer();
        List<String> entityTypesList = ImmutableList.of("ORGANIZATION", "USER", "TICKET");
        Assertions.assertThat(renderer.renderEntityTypesList(entityTypesList)).isTrue();
    }

}