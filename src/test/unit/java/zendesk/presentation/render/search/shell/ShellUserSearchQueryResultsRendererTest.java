package zendesk.presentation.render.search.shell;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import zendesk.models.data.Organization;
import zendesk.models.data.User;
import zendesk.models.service.query.search.OrganizationSearchResultEntry;
import zendesk.models.service.query.search.SearchQueryResultEntry;
import zendesk.models.service.query.search.UserSearchResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;

public class ShellUserSearchQueryResultsRendererTest {
    @Test
    public void resultsStartShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellUserSearchQueryResultsRenderer(new ApplicationConfiguration());
        Assertions.assertThat(renderer.resultsStart()).isTrue();
    }

    @Test
    public void enterResultShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellUserSearchQueryResultsRenderer(new ApplicationConfiguration());
        User sourceData = new User("1", "http://initech.zendesk.com/api/v2/users/1.json",
                "74341f74-9c79-49d5-9611-87ef9b6eb75f", "Francisca Rasmussen", "Miss Coffey",
                "2016-04-15T05:19:46 -10:00", "true", "true", "false", "en-AU", "Sri Lanka",
                "2013-08-04T01:03:27 -10:00", "coffeyrasmussen@flotonic.com", "8335-422-718", "Don't Worry Be Happy!",
                "119", ImmutableList.of(), "true", "admin");
        SearchQueryResultEntry result = new UserSearchResultEntry(sourceData, "Fred CO", ImmutableList.of());
        Assertions.assertThat(renderer.enterResult(result)).isTrue();
    }

    @Test
    public void enterResultShouldFailWhenGivenIncorrectResultType() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellUserSearchQueryResultsRenderer(new ApplicationConfiguration());
        Organization sourceData = new Organization("101", "http://initech.zendesk.com/api/v2/organizations/101.json",
                "9270ed79-35eb-4a38-a46f-35725197ea8d", "Enthaze",
                ImmutableList.of(), "2016-05-21T11:10:28 -10:00", "MegaCorp", "false",
                ImmutableList.of());
        SearchQueryResultEntry result = new OrganizationSearchResultEntry(sourceData, ImmutableList.of(),
                ImmutableList.of());
        Assertions.assertThat(renderer.enterResult(result)).isFalse();
    }

    @Test
    public void resultsEndShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellUserSearchQueryResultsRenderer(new ApplicationConfiguration());
        Assertions.assertThat(renderer.resultsEnd()).isTrue();
    }
}