package zendesk.presentation.render.search.shell;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import zendesk.models.data.Organization;
import zendesk.models.data.Ticket;
import zendesk.models.service.query.search.OrganizationSearchResultEntry;
import zendesk.models.service.query.search.SearchQueryResultEntry;
import zendesk.models.service.query.search.TicketSearchResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;

import java.util.List;

public class ShellOrganizationSearchQueryResultsRendererTest {
    @Test
    public void resultsStartShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellOrganizationSearchQueryResultsRenderer(new ApplicationConfiguration());
        Assertions.assertThat(renderer.resultsStart()).isTrue();
    }

    @Test
    public void enterResultShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellOrganizationSearchQueryResultsRenderer(new ApplicationConfiguration());
        List<String> organizationUsers = ImmutableList.of("fred", "george");
        List<String> organizationTickets =ImmutableList.of("fix stuff", "fix other stuff");
        Organization sourceData = new Organization("101", "http://initech.zendesk.com/api/v2/organizations/101.json",
                "9270ed79-35eb-4a38-a46f-35725197ea8d", "Enthaze",
                ImmutableList.of("kage.com", "ecratic.com", "endipin.com", "zentix.com", "zentix.com",
                        "zentix.com", "zentix.com"), "2016-05-21T11:10:28 -10:00", "MegaCorp", "false",
                ImmutableList.of("Fulton", "West", "Rodriguez"));
        SearchQueryResultEntry result = new OrganizationSearchResultEntry(sourceData, organizationUsers,
                organizationTickets);
        Assertions.assertThat(renderer.enterResult(result)).isTrue();
    }

    @Test
    public void enterResultShouldFailWhenGivenIncorrectResultType() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellOrganizationSearchQueryResultsRenderer(new ApplicationConfiguration());
        Ticket sourceData = new Ticket("436bf9b0-1147-4c0a-8439-6f79833bff5b",
                "http://initech.zendesk.com/api/v2/tickets/436bf9b0-1147-4c0a-8439-6f79833bff5b.json",
                "9210cdc9-4bee-485f-a078-35396cd74063", "2016-04-28T11:19:34 -10:00", "incident",
                "A Catastrophe in Korea (North)", "Nostrud ad sit velit cupidatat", "high", "pending", "38", "24",
                "116", ImmutableList.of(), "false", "2016-07-31T02:37:50 -10:00", "web");
        SearchQueryResultEntry result = new TicketSearchResultEntry(sourceData, "Fred", "Bob", "Fred CO!");
        Assertions.assertThat(renderer.enterResult(result)).isFalse();
    }

    @Test
    public void resultsEndShouldNotFail() throws Exception {
        SearchQueryResultsRenderer renderer = new ShellOrganizationSearchQueryResultsRenderer(new ApplicationConfiguration());
        Assertions.assertThat(renderer.resultsEnd()).isTrue();
    }
}