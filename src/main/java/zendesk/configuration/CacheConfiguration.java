package zendesk.configuration;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configures the method result cache used in various parts of the application.
 */
@Configuration
@EnableCaching
public class CacheConfiguration extends CachingConfigurerSupport {
    @Bean
    @Override
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("EntityDataValidation", "ConfigurationValidation",
                "CommandEntityFieldParamValidation");
    }
}
