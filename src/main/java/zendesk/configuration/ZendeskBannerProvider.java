package zendesk.configuration;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.BannerProvider;
import org.springframework.stereotype.Component;

/**
 * Customises the banner message that gets printed when the applicaiton starts.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ZendeskBannerProvider implements BannerProvider {

    private static final String BANNER = " ________   _______    ________    ________   _______    ________   ___  __            ________   ________   ________   \n"
            + "|\\_____  \\ |\\  ___ \\  |\\   ___  \\ |\\   ___ \\ |\\  ___ \\  |\\   ____\\ |\\  \\|\\  \\         |\\   __  \\ |\\   __  \\ |\\   __  \\  \n"
            + " \\|___/  /|\\ \\   __/| \\ \\  \\\\ \\  \\\\ \\  \\_|\\ \\\\ \\   __/| \\ \\  \\___|_\\ \\  \\/  /|_       \\ \\  \\|\\  \\\\ \\  \\|\\  \\\\ \\  \\|\\  \\ \n"
            + "     /  / / \\ \\  \\_|/__\\ \\  \\\\ \\  \\\\ \\  \\ \\\\ \\\\ \\  \\_|/__\\ \\_____  \\\\ \\   ___  \\       \\ \\   __  \\\\ \\   ____\\\\ \\   ____\\\n"
            + "    /  /_/__ \\ \\  \\_|\\ \\\\ \\  \\\\ \\  \\\\ \\  \\_\\\\ \\\\ \\  \\_|\\ \\\\|____|\\  \\\\ \\  \\\\ \\  \\       \\ \\  \\ \\  \\\\ \\  \\___| \\ \\  \\___|\n"
            + "   |\\________\\\\ \\_______\\\\ \\__\\\\ \\__\\\\ \\_______\\\\ \\_______\\ ____\\_\\  \\\\ \\__\\\\ \\__\\       \\ \\__\\ \\__\\\\ \\__\\     \\ \\__\\   \n"
            + "    \\|_______| \\|_______| \\|__| \\|__| \\|_______| \\|_______||\\_________\\\\|__| \\|__|        \\|__|\\|__| \\|__|      \\|__|   \n"
            + "                                                           \\|_________|                                                 \n"
            + "                                                                                                                        \n"
            + "                                                                                                                        ";


    private static final String WELCOME = "Welcome to the Zendesk application. For assistance hit TAB or type \"help\".";


    @Override
    public String getBanner() {
        return BANNER;
    }

    @Override
    public String getVersion() {
        return "v 1.0";
    }

    @Override
    public String getWelcomeMessage() {
        return WELCOME;
    }

    @Override
    public String getProviderName() {
        return "zendesk";
    }
}
