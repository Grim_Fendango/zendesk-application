package zendesk.models.service.query.schema;

import java.util.List;

/**
 * Model that caputures the schema of an source data entity.
 */
public class SchemaQueryResult {
    private final String entityType;
    private final List<String> fields;

    public SchemaQueryResult(String entityType, List<String> fields) {
        this.entityType = entityType;
        this.fields = fields;
    }

    public String getEntityType() {
        return entityType;
    }

    public List<String> getFields() {
        return fields;
    }
}
