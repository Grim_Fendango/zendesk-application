package zendesk.models.service.query.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import zendesk.models.data.Organization;

import java.util.List;

/**
 * Model that captures the results of a organization based search query. This includes joined values.
 */
public class OrganizationSearchResultEntry implements SearchQueryResultEntry {
    private String id;

    private String url;

    private String externalId;

    private String name;

    private List<String> domainNames;

    private String createdAt;

    private String details;

    private String sharedTickets;

    private List<String> tags;

    private List<String> users;

    private List<String> tickets;

    public OrganizationSearchResultEntry(Organization sourceModel, List<String> users, List<String> tickets) {
        if (sourceModel != null && users != null && tickets != null) {
            this.id = sourceModel.getId();
            this.url = sourceModel.getUrl();
            this.externalId = sourceModel.getExternalId();
            this.name = sourceModel.getName();
            this.domainNames = sourceModel.getDomainNames();
            this.createdAt = sourceModel.getCreatedAt();
            this.details = sourceModel.getDetails();
            this.sharedTickets = sourceModel.getSharedTickets();
            this.tags = sourceModel.getTags();
            this.users = users;
            this.tickets = tickets;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("domain_names")
    public List<String> getDomainNames() {
        return domainNames;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("shared_tickets")
    public String getSharedTickets() {
        return sharedTickets;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("users")
    public List<String> getUsers() {
        return users;
    }

    @JsonProperty("tickets")
    public List<String> getTickets() {
        return tickets;
    }
}
