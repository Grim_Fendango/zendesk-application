package zendesk.models.service.query.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import zendesk.models.data.Ticket;

import java.util.List;

/**
 * Model that captures the results of a ticket based search query. This includes joined values.
 */
public class TicketSearchResultEntry implements SearchQueryResultEntry {
    private String id;

    private String url;

    private String externalId;

    private String createdAt;

    private String type;

    private String subject;

    private String description;

    private String priority;

    private String status;

    private String submitterId;

    private String submitterUserName;

    private String assigneeId;

    private String assigneeUserName;

    private String organizationId;

    private String organizationName;

    private List<String> tags;

    private String hasIncidents;

    private String dueAt;

    private String via;

    public TicketSearchResultEntry(Ticket sourceModel, String submitterUserName, String assigneeUserName,
            String organizationName) {
        this.id = sourceModel.getId();
        this.url = sourceModel.getUrl();
        this.externalId = sourceModel.getExternalId();
        this.createdAt = sourceModel.getCreatedAt();
        this.type = sourceModel.getType();
        this.subject = sourceModel.getSubject();
        this.description = sourceModel.getDescription();
        this.priority = sourceModel.getPriority();
        this.status =  sourceModel.getStatus();
        this.submitterId = sourceModel.getSubmitterId();
        this.assigneeId = sourceModel.getAssigneeId();
        this.organizationId = sourceModel.getOrganizationId();
        this.tags = sourceModel.getTags();
        this.hasIncidents = sourceModel.getHasIncidents();
        this.dueAt = sourceModel.getDueAt();
        this.via = sourceModel.getVia();

        this.submitterUserName = submitterUserName;
        this.assigneeUserName = assigneeUserName;
        this.organizationName = organizationName;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    } // IS NULL?!?

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("priority")
    public String getPriority() {
        return priority;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("submitter_id")
    public String getSubmitterId() {
        return submitterId;
    }

    @JsonProperty("submitter_name")
    public String getSubmitterUserName() {
        return submitterUserName;
    }

    @JsonProperty("assignee_id")
    public String getAssigneeId() {
        return assigneeId;
    }

    @JsonProperty("assignee_name")
    public String getAssigneeUserName() {
        return assigneeUserName;
    }

    @JsonProperty("organization_id")
    public String getOrganizationId() {
        return organizationId;
    }

    @JsonProperty("organization_name")
    public String getOrganizationName() {
        return organizationName;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("has_incidents")
    public String getHasIncidents() {
        return hasIncidents;
    }

    @JsonProperty("due_at")
    public String getDueAt() {
        return dueAt;
    }

    @JsonProperty("via")
    public String getVia() {
        return via;
    }
}
