package zendesk.models.service.query.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import zendesk.models.data.User;

import java.util.List;

/**
 * Model that captures the results of a user based search query. This includes joined values.
 */
public class UserSearchResultEntry implements SearchQueryResultEntry {

    private String id;

    private String url;

    private String externalId;

    private String name;

    private String alias;

    private String createdAt;

    private String active;

    private String verified;

    private String shared;

    private String locale;

    private String timezone;

    private String lastLoginAt;

    private String email;

    private String phone;

    private String signature;

    private String organizationId;

    private String organizationName;

    private List<String> tags;

    private String suspended;

    private String role;

    private List<String> tickets;

    public UserSearchResultEntry(User sourceModel, String organizationName, List<String> tickets) {
        this.id = sourceModel.getId();
        this.url = sourceModel.getUrl();
        this.externalId = sourceModel.getExternalId();
        this.name = sourceModel.getName();
        this.alias = sourceModel.getAlias();
        this.createdAt = sourceModel.getCreatedAt();
        this.active = sourceModel.getActive();
        this.verified = sourceModel.getVerified();
        this.shared = sourceModel.getShared();
        this.locale = sourceModel.getLocale();
        this.timezone = sourceModel.getTimezone();
        this.lastLoginAt = sourceModel.getLastLoginAt();
        this.email = sourceModel.getEmail();
        this.phone = sourceModel.getPhone();
        this.signature = sourceModel.getSignature();
        this.organizationId = sourceModel.getOrganizationId();
        this.tags = sourceModel.getTags();
        this.suspended = sourceModel.getSuspended();
        this.role = sourceModel.getRole();

        this.organizationName = organizationName;
        this.tickets = tickets;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("verified")
    public String getVerified() {
        return verified;
    }

    @JsonProperty("shared")
    public String getShared() {
        return shared;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    @JsonProperty("last_login_at")
    public String getLastLoginAt() {
        return lastLoginAt;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @JsonProperty("organization_id")
    public String getOrganizationId() {
        return organizationId;
    }

    @JsonProperty("organization_name")
    public String getOrganizationName() {
        return organizationName;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("suspended")
    public String getSuspended() {
        return suspended;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("tickets")
    public List<String> getTickets() {
        return tickets;
    }
}
