package zendesk.models.service.query.search;

/**
 * Null interface used to identify search query results output models.
 */
public interface SearchQueryResultEntry {
}
