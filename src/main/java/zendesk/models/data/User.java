package zendesk.models.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Source data model for the user entity type.
 */
public class User implements SourceDataEntity{
    private String id;

    private String url;

    private String externalId;

    private String name;

    private String alias;

    private String createdAt;

    private String active;

    private String verified;

    private String shared;

    private String locale;

    private String timezone;

    private String lastLoginAt;

    private String email;

    private String phone;

    private String signature;

    private String organizationId;

    private List<String> tags;

    private String suspended;

    private String role;

    public User() {}

    public User(String id, String url, String externalId, String name, String alias, String createdAt, String active,
            String verified, String shared, String locale, String timezone, String lastLoginAt, String email,
            String phone, String signature, String organizationId, List<String> tags, String suspended, String role) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.name = name;
        this.alias = alias;
        this.createdAt = createdAt;
        this.active = active;
        this.verified = verified;
        this.shared = shared;
        this.locale = locale;
        this.timezone = timezone;
        this.lastLoginAt = lastLoginAt;
        this.email = email;
        this.phone = phone;
        this.signature = signature;
        this.organizationId = organizationId;
        this.tags = tags;
        this.suspended = suspended;
        this.role = role;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("verified")
    public String getVerified() {
        return verified;
    }

    @JsonProperty("shared")
    public String getShared() {
        return shared;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    @JsonProperty("last_login_at")
    public String getLastLoginAt() {
        return lastLoginAt;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @JsonProperty("organization_id")
    public String getOrganizationId() {
        return organizationId;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("suspended")
    public String getSuspended() {
        return suspended;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }
}