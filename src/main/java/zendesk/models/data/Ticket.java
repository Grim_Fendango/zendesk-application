package zendesk.models.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Source data model for the ticket entity type.
 */
public class Ticket implements SourceDataEntity {
    private String id;

    private String url;

    private String externalId;

    private String createdAt;

    private String type;

    private String subject;

    private String description;

    private String priority;

    private String status;

    private String submitterId;

    private String assigneeId;

    private String organizationId;

    private List<String> tags;

    private String hasIncidents;

    private String dueAt;

    private String via;

    public Ticket() {}

    public Ticket(String id, String url, String externalId, String createdAt, String type, String subject,
            String description, String priority, String status, String submitterId, String assigneeId,
            String organizationId, List<String> tags, String hasIncidents, String dueAt, String via) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.createdAt = createdAt;
        this.type = type;
        this.subject = subject;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.submitterId = submitterId;
        this.assigneeId = assigneeId;
        this.organizationId = organizationId;
        this.tags = tags;
        this.hasIncidents = hasIncidents;
        this.dueAt = dueAt;
        this.via = via;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("priority")
    public String getPriority() {
        return priority;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("submitter_id")
    public String getSubmitterId() {
        return submitterId;
    }

    @JsonProperty("assignee_id")
    public String getAssigneeId() {
        return assigneeId;
    }

    @JsonProperty("organization_id")
    public String getOrganizationId() {
        return organizationId;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("has_incidents")
    public String getHasIncidents() {
        return hasIncidents;
    }

    @JsonProperty("due_at")
    public String getDueAt() {
        return dueAt;
    }

    @JsonProperty("via")
    public String getVia() {
        return via;
    }
}
