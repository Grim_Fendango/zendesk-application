package zendesk.models.data;

/**
 * Null interface used to identify source data models.
 */
public interface SourceDataEntity {

}
