package zendesk.models.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Source data model for the organization entity type.
 */
public class Organization implements SourceDataEntity {

    private String id;

    private String url;

    private String externalId;

    private String name;

    private List<String> domainNames;

    private String createdAt;

    private String details;

    private String sharedTickets;

    private List<String> tags;

    public Organization() {

    }

    public Organization(String id, String url, String externalId, String name, List<String> domainNames,
            String createdAt, String details, String sharedTickets, List<String> tags) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.name = name;
        this.domainNames = domainNames;
        this.createdAt = createdAt;
        this.details = details;
        this.sharedTickets = sharedTickets;
        this.tags = tags;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("domain_names")
    public List<String> getDomainNames() {
        return domainNames;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("shared_tickets")
    public String getSharedTickets() {
        return sharedTickets;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }
}
