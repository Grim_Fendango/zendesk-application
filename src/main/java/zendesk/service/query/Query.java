package zendesk.service.query;

/**
 * Defines a common interface between all of the query types
 */
public interface Query {

    /**
     * @return True: if query was successful
     */
    boolean run();

    int numberOfResults();
}
