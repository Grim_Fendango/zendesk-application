package zendesk.service.query.schema.json;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.google.common.collect.ImmutableList;
import zendesk.models.data.Organization;
import zendesk.models.data.Ticket;
import zendesk.models.data.User;
import zendesk.models.service.query.schema.SchemaQueryResult;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.schema.SchemaQuery;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Performs a schema query where the source data is in the JSON format.
 *
 * Schema queries query the sctructure of the source data. Currently this can only provide a list of the searchable
 * fields of one or more entities. This current implementation uses reflection based approach on the source data models
 * to determine what fields are available.
 */
public class JSONSchemaQuery implements SchemaQuery {
    private final Optional<String> entityType;
    private final ApplicationConfiguration configuration;
    private final Optional<SchemaQueryResultsRenderer> renderer;

    private List<SchemaQueryResult> results = ImmutableList.of(); // empty

    public JSONSchemaQuery(String entityType, ApplicationConfiguration configuration, SchemaQueryResultsRenderer renderer) {
        this.entityType = Optional.ofNullable(entityType);
        this.configuration = configuration;
        this.renderer = Optional.ofNullable(renderer);
    }

    @Override
    public boolean run() {
        if (configuration.isValid()) {
            if (entityType.isPresent()) {
                results = ImmutableList.of(getSchemaQueryResult(entityType.get()));
                renderer.ifPresent(r -> r.renderResult(results.get(0)));
            } else {
                results = configuration.getSupportedEntityTypes().stream()
                        .map(entity -> getSchemaQueryResult(entity))
                        .collect(Collectors.toList());
                renderer.ifPresent(r -> r.renderResults(results));
            }
            return true;
        }
        return false;
    }

    private SchemaQueryResult getSchemaQueryResult(String entityName) {
        switch (entityName) {
            case ApplicationConfiguration.ORGANIZATION:
                return new SchemaQueryResult(entityName, getEntityTypeFieldNames(Organization.class));
            case ApplicationConfiguration.USER:
                return new SchemaQueryResult(entityName, getEntityTypeFieldNames(User.class));
            case ApplicationConfiguration.TICKET:
                return new SchemaQueryResult(entityName, getEntityTypeFieldNames(Ticket.class));
            default:
                // This should never happen
                return new SchemaQueryResult(entityName, ImmutableList.of());
        }
    }

    private List<String> getEntityTypeFieldNames(Type type) {
        ObjectMapper mapper = new ObjectMapper();
        JavaType jsonType = mapper.getTypeFactory().constructType(type);
        BeanDescription introspection =
                mapper.getSerializationConfig().introspect(jsonType);
        List<BeanPropertyDefinition> properties = introspection.findProperties();
        return properties.stream().map(property -> property.getName()).collect(Collectors.toList());
    }

    @Override
    public int numberOfResults() {
        return results.size();
    }

    @Override
    public List<SchemaQueryResult> getResults() {
        return results;
    }
}
