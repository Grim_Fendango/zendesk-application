package zendesk.service.query.schema;

import jline.internal.Nullable;
import org.springframework.stereotype.Component;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.schema.json.JSONSchemaQuery;

/**
 * Used to get schema schema queries that are appropriate for the configured source format.
 */
@Component
public class SchemaQueryFactory {

    /**
     * @param entityType The entity type to query. This must be a valid entity type.
     * @param configuration The application configuration, this must be valid.
     * @param renderer The results renderer to use, if null the results will not be rendered.
     * @return SchemaQuery for a specific entity type that is appropriate for the configured source data format.
     *         Null if configuration is invalid, the entity type does not exist or a suitable schema query could not be
     *         found.
     */
    public SchemaQuery getQuery(String entityType, ApplicationConfiguration configuration,
            @Nullable SchemaQueryResultsRenderer renderer) {

        return _getQuery(entityType.toUpperCase(), configuration, renderer);
    }

    /**
     * @param configuration The application configuration, this must be valid.
     * @param renderer The results renderer to use, if null the results will not be rendered.
     * @return SchemaQuery for all entity types that is appropriate for the configured source format.
     *         Null if configuration is invalid, the entity type does not exist or a suitable schema query could not be
     *         found.
     */
    public SchemaQuery getQueryForAllEntities(ApplicationConfiguration configuration,
            @Nullable SchemaQueryResultsRenderer renderer) {
        return _getQuery(null, configuration, renderer);
    }

    private SchemaQuery _getQuery(String entityType, ApplicationConfiguration configuration,
            SchemaQueryResultsRenderer renderer) {
        if (configuration.isValid()) {
            switch (configuration.getSourceDataFormat()) {
                case ApplicationConfiguration.JSON:
                    return new JSONSchemaQuery(entityType, configuration, renderer);
            }
        }
        return null;
    }
}
