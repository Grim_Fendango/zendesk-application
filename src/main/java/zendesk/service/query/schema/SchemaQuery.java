package zendesk.service.query.schema;

import zendesk.models.service.query.schema.SchemaQueryResult;
import zendesk.service.query.Query;

import java.util.List;

/**
 * Schema queries query the sctructure of the source data. Currently this can only provide a list of the searchable
 * fields of one or more entities.
 */
public interface SchemaQuery extends Query {
    List<SchemaQueryResult> getResults();
}
