package zendesk.service.query.search;

import org.springframework.stereotype.Component;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.json.OrganizationJSONSearchQuery;
import zendesk.service.query.search.json.TicketJSONSearchQuery;
import zendesk.service.query.search.json.UserJSONSearchQuery;

/**
 * Used to get search queries that are appropriate for the configured source format and the entity type being
 * queried.
 */
@Component
public class SearchQueryFactory {

    /**
     * @param entityType The entity type to query. This must be a valid entity type.
     * @param field The field of entity that will be compared against valueToMatch
     * @param valueToMatch The value of the given field that search results will have.
     * @param configuration The application configuration, this must be valid.
     * @param renderer The results renderer to use, if null the results will not be rendered.
     * @return SearchQuery that is appropriate for the configured source data format and entity type being queried.
     *         Null if configuration is invalid or a suitable search query could not be found.
     */
    public SearchQuery getQuery(String entityType, String field, String valueToMatch,
            ApplicationConfiguration configuration, SearchQueryResultsRenderer renderer) {

        if (configuration.isValid()) {
            switch (entityType) {
                case ApplicationConfiguration.ORGANIZATION:
                    return new OrganizationJSONSearchQuery(field, valueToMatch, configuration, renderer);
                case ApplicationConfiguration.USER:
                    return new UserJSONSearchQuery(field, valueToMatch, configuration, renderer);
                case ApplicationConfiguration.TICKET:
                    return new TicketJSONSearchQuery(field, valueToMatch, configuration, renderer);
            }
        }

        return null;
    }
}
