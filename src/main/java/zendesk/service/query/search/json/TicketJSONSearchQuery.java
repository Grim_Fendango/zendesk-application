package zendesk.service.query.search.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import zendesk.models.data.Ticket;
import zendesk.models.service.query.search.TicketSearchResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.SearchQuery;

import java.io.IOException;

/**
 * Queries the ticket source data set and renders results that matches the given selection criteria.
 */
public class TicketJSONSearchQuery implements SearchQuery {

    private final String field;
    private final String valueToMatch;
    private final ApplicationConfiguration configuration;
    private final SearchQueryResultsRenderer renderer;
    private SearchQueryJSONArrayCursor organizationJSONArrayCursor;
    private SearchQueryJSONArrayCursor userJSONArrayCursor;
    private SearchQueryJSONArrayCursor ticketJSONArrayCursor;

    public TicketJSONSearchQuery(String field, String valueToMatch, ApplicationConfiguration configuration,
            SearchQueryResultsRenderer renderer) {
        this.field = field;
        this.valueToMatch = valueToMatch;
        this.configuration = configuration;
        this.renderer = renderer;
        this.organizationJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getOrganizationDataPath());
        this.userJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getUserDataPath());
        this.ticketJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getTicketDataPath());
    }

    public TicketJSONSearchQuery(String field, String valueToMatch, ApplicationConfiguration configuration,
            SearchQueryResultsRenderer renderer, SearchQueryJSONArrayCursor organizationJSONArrayCursor,
            SearchQueryJSONArrayCursor userJSONArrayCursor, SearchQueryJSONArrayCursor ticketJSONArrayCursor) {
        this.field = field;
        this.valueToMatch = valueToMatch;
        this.configuration = configuration;
        this.renderer = renderer;
        this.organizationJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getOrganizationDataPath());
        this.userJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getUserDataPath(),
                configuration.getMaxJoinedResults());
        this.ticketJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getTicketDataPath(),
                configuration.getMaxJoinedResults());
    }

    @Override
    public boolean run() {
        if (!configuration.isValid()) {
            return false;
        }

        if(!renderer.resultsStart()) {
            return false;
        }

        try {
            ticketJSONArrayCursor.start();
            ObjectMapper mapper = new ObjectMapper();
            while (ticketJSONArrayCursor.nextMatchingNode(field, valueToMatch)) {
                // Find matching entity result
                JsonNode entityNode = ticketJSONArrayCursor.getCurrentEntityNode();
                Ticket ticket = mapper.treeToValue(entityNode, Ticket.class);

                // Find join values
                userJSONArrayCursor.start();
                String submitterUserName = "N/A";
                if (userJSONArrayCursor.nextMatchingNode("_id", ticket.getSubmitterId())) {
                    JsonNode userNode = userJSONArrayCursor.getCurrentEntityNode();
                    if (userNode.has("name")) {
                        submitterUserName = userNode.get("name").textValue();
                    }
                }
                userJSONArrayCursor.start();
                String assigneeUserName = "N/A";
                if (userJSONArrayCursor.nextMatchingNode("_id", ticket.getAssigneeId())) {
                    JsonNode userNode = userJSONArrayCursor.getCurrentEntityNode();
                    if (userNode.has("name")) {
                        assigneeUserName = userNode.get("name").textValue();
                    }
                }
                organizationJSONArrayCursor.start();
                String organizationName = "N/A";
                if (organizationJSONArrayCursor.nextMatchingNode("_id", ticket.getOrganizationId())) {
                    JsonNode organizationNode = organizationJSONArrayCursor.getCurrentEntityNode();
                    if (organizationNode.has("name")) {
                        organizationName = organizationNode.get("name").textValue();
                    }
                }

                if (!renderer.enterResult(
                        new TicketSearchResultEntry(ticket, submitterUserName, assigneeUserName, organizationName))) {
                    return false;
                }
            }

            organizationJSONArrayCursor.close();
            userJSONArrayCursor.close();
            ticketJSONArrayCursor.close();
        } catch (IOException e) {
            return false;
        }

        if(!renderer.resultsEnd()) {
            return false;
        }

        return true;
    }

    @Override
    public int numberOfResults() {
        return ticketJSONArrayCursor.getMatchesFound();
    }

    @Override
    public String getEntityType() {
        return ApplicationConfiguration.TICKET;
    }
}
