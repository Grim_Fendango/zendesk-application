package zendesk.service.query.search.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import jline.internal.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * This is a JSON curser that allows for traversal to the next json node that meets the selection criteria.
 */
public class SearchQueryJSONArrayCursor {

    private JsonParser jsonParser;
    private final String jsonFilePath;
    private JsonNode currentEntityNode;
    private final ObjectMapper mapper = new ObjectMapper();

    private int matchesFound = 0;
    private final Optional<Integer> maximumMatches;

    /**
     * @param jsonFilePath The path of the json to use for searching
     */
    public SearchQueryJSONArrayCursor(String jsonFilePath) {
        this.jsonFilePath = jsonFilePath;
        this.maximumMatches = Optional.empty();
    }

    /**
     * @param jsonFilePath The path of the json to use for searching
     * @param maximumMatches The maximum number of matches to find. Infinite number of matches can be found when
     *                       this is null.
     */
    public SearchQueryJSONArrayCursor(String jsonFilePath, @Nullable Integer maximumMatches) {
        this.jsonFilePath = jsonFilePath;
        this.maximumMatches = Optional.of(maximumMatches);
    }

    /**
     * Moves the cursor to the start of the file. This also resets the matchesFound count.
     * The current node will be set to null (before any results have been found).
     * @return True: if was successful, False: if not array at first level of json.
     */
    public boolean start() throws IOException {
        if (jsonParser != null) {
            jsonParser.close();
        }
        jsonParser = (new MappingJsonFactory()).createJsonParser(new File(jsonFilePath));
        JsonToken currentToken = jsonParser.nextToken();
        if (currentToken != JsonToken.START_ARRAY) {
            return false; // JSON should be an array
        }
        currentEntityNode = null;
        matchesFound = 0;
        return true;
    }

    /**
     * Moves the curser to the next matching node (if there is one). If the curser has not been started
     * this calls is as a convenience. If no matches are found the current element will be set to null.
     *
     * @param field The field with the value that will be compared against valueToMatch.
     * @param valueToMatch The value to match
     * @return True: if new match found and false if no more matches were found in the rest of the json data.
     * @throws IOException
     */
    public boolean nextMatchingNode(String field, String valueToMatch) throws IOException {
        if (jsonParser == null) {
            if(!start()) {
                return false;
            }
        }

        while (!maximumMatchesFound() &&  jsonParser.nextToken() != JsonToken.END_ARRAY) {
            JsonNode entityNode = jsonParser.readValueAsTree();
            if (entityNode != null && isMatch(entityNode, field, valueToMatch)) {
                currentEntityNode = entityNode;
                matchesFound++;
                return true;
            }
        }
        currentEntityNode = null;
        return false;
    }

    /**
     *
     * @return The current node that the curser is pointing to. Returns null if curser has not been startd.
     */
    public JsonNode getCurrentEntityNode() {
        return currentEntityNode;
    }

    /**
     * Closes the json file. Moves cursor to end.
     * @throws IOException
     */
    public void close() throws IOException {
        if (jsonParser != null) {
            jsonParser.close();
        }
    }

    /**
     * @return The number of matches that have been found
     */
    public int getMatchesFound() {
        return matchesFound;
    }

    private boolean maximumMatchesFound() {
        if (maximumMatches.isPresent()) {
            return maximumMatches.get().intValue() == matchesFound;
        }
        return false;
    }

    private String getJsonFilePath() {
        return jsonFilePath;
    }

    /**
     * @param entityNode JSON object node to search
     * @param field The field with the value that will be compared against valueToMatch.
     * @param valueToMatch The value to match
     * @return True: if entityNode contains a field that with a value equal to valueToMatch
     */
    private boolean isMatch(JsonNode entityNode, String field, String valueToMatch) {
        if (!entityNode.isObject()) {
            return false;
        }

        boolean isMatch = false;
        JsonNode fieldNode = entityNode.get(field);
        if (fieldNode != null) {
            if (JsonNodeType.ARRAY.equals(fieldNode.getNodeType())) {
                for (JsonNode node : fieldNode) {
                    if (node.isValueNode() && node.asText("").equals(valueToMatch)) {
                        isMatch = true;
                        break;
                    }
                }
            } else if (fieldNode.isValueNode()) {
                isMatch = fieldNode.asText("").equals(valueToMatch);
            }
        }
        return isMatch;
    }
}
