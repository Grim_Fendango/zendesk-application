package zendesk.service.query.search.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import zendesk.models.data.Organization;
import zendesk.models.service.query.search.OrganizationSearchResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.SearchQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Queries the organization source data set and renders results that matches the given selection criteria.
 */
public class OrganizationJSONSearchQuery implements SearchQuery {

    private final String field;
    private final String valueToMatch;
    private final ApplicationConfiguration configuration;
    private final SearchQueryResultsRenderer renderer;
    private SearchQueryJSONArrayCursor organizationJSONArrayCursor;
    private SearchQueryJSONArrayCursor userJSONArrayCursor;
    private SearchQueryJSONArrayCursor ticketJSONArrayCursor;

    public OrganizationJSONSearchQuery(String field, String valueToMatch, ApplicationConfiguration configuration,
            SearchQueryResultsRenderer renderer) {
        this.field = field;
        this.valueToMatch = valueToMatch;
        this.configuration = configuration;
        this.renderer = renderer;
        this.organizationJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getOrganizationDataPath());
        this.userJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getUserDataPath(),
                configuration.getMaxJoinedResults());
        this.ticketJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getTicketDataPath(),
                configuration.getMaxJoinedResults());
    }

    public OrganizationJSONSearchQuery(String field, String valueToMatch, ApplicationConfiguration configuration,
            SearchQueryResultsRenderer renderer, SearchQueryJSONArrayCursor organizationJSONArrayCursor,
            SearchQueryJSONArrayCursor userJSONArrayCursor, SearchQueryJSONArrayCursor ticketJSONArrayCursor) {
        this.field = field;
        this.valueToMatch = valueToMatch;
        this.configuration = configuration;
        this.renderer = renderer;
        this.organizationJSONArrayCursor = organizationJSONArrayCursor;
        this.userJSONArrayCursor = userJSONArrayCursor;
        this.ticketJSONArrayCursor = ticketJSONArrayCursor;
    }

    @Override
    public boolean run() {
        if (!configuration.isValid()) {
            return false;
        }
        if(!renderer.resultsStart()) {
            return false;
        }

        try {
            organizationJSONArrayCursor.start();
            ObjectMapper mapper = new ObjectMapper();
            while (organizationJSONArrayCursor.nextMatchingNode(field, valueToMatch)) {
                // Find matching entity result
                JsonNode entityNode = organizationJSONArrayCursor.getCurrentEntityNode();
                Organization organization = mapper.treeToValue(entityNode, Organization.class);

                // Find join values
                userJSONArrayCursor.start();
                List<String> userNames = new ArrayList<>();
                while (userJSONArrayCursor.nextMatchingNode("organization_id", organization.getId())) {
                    JsonNode userNode = userJSONArrayCursor.getCurrentEntityNode();
                    userNames.add(userNode.has("name") ? userNode.get("name").textValue() : "N/A");
                }
                ticketJSONArrayCursor.start();
                List<String> ticketSubjects = new ArrayList<>();
                while (ticketJSONArrayCursor.nextMatchingNode("organization_id", organization.getId())) {
                    JsonNode ticketNode = ticketJSONArrayCursor.getCurrentEntityNode();
                    ticketSubjects.add(ticketNode.has("subject") ? ticketNode.get("subject").textValue() : "N/A");
                }

                if (!renderer.enterResult(new OrganizationSearchResultEntry(organization, userNames, ticketSubjects))) {
                    return false;
                }
            }

            organizationJSONArrayCursor.close();
            userJSONArrayCursor.close();
            ticketJSONArrayCursor.close();
        } catch (IOException e) {
            return false;
        }

        if(!renderer.resultsEnd()) {
            return false;
        }

        return true;
    }

    @Override
    public int numberOfResults() {
        return organizationJSONArrayCursor.getMatchesFound();
    }

    @Override
    public String getEntityType() {
        return ApplicationConfiguration.ORGANIZATION;
    }

}
