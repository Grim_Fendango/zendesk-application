package zendesk.service.query.search.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import zendesk.models.data.User;
import zendesk.models.service.query.search.UserSearchResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.SearchQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Queries the user source data set and renders results that matches the given selection criteria.
 */
public class UserJSONSearchQuery implements SearchQuery {

    private final String field;
    private final String valueToMatch;
    private final ApplicationConfiguration configuration;
    private final SearchQueryResultsRenderer renderer;
    private SearchQueryJSONArrayCursor organizationJSONArrayCursor;
    private SearchQueryJSONArrayCursor userJSONArrayCursor;
    private SearchQueryJSONArrayCursor ticketJSONArrayCursor;

    public UserJSONSearchQuery(String field, String valueToMatch, ApplicationConfiguration configuration,
            SearchQueryResultsRenderer renderer) {
        this.field = field;
        this.valueToMatch = valueToMatch;
        this.configuration = configuration;
        this.renderer = renderer;
        this.organizationJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getOrganizationDataPath());
        this.userJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getUserDataPath());
        this.ticketJSONArrayCursor = new SearchQueryJSONArrayCursor(configuration.getTicketDataPath(),
                configuration.getMaxJoinedResults());
    }

    @Override
    public boolean run() {
        if (!configuration.isValid()) {
            return false;
        }

        if(!renderer.resultsStart()) {
            return false;
        }

        try {
            userJSONArrayCursor.start();
            ObjectMapper mapper = new ObjectMapper();
            while (userJSONArrayCursor.nextMatchingNode(field, valueToMatch)) {
                // Find matching entity result
                JsonNode entityNode = userJSONArrayCursor.getCurrentEntityNode();
                User user = mapper.treeToValue(entityNode, User.class);

                // Find join values
                organizationJSONArrayCursor.start();
                String organizationName = "N/A";
                if (organizationJSONArrayCursor.nextMatchingNode("_id", user.getOrganizationId())) {
                    JsonNode organizationNode = organizationJSONArrayCursor.getCurrentEntityNode();
                    if (organizationNode.has("name")) {
                        organizationName = organizationNode.get("name").textValue();
                    }
                }
                List<String> ticketSubjects = new ArrayList<>();
                ticketJSONArrayCursor.start();
                while (ticketJSONArrayCursor.nextMatchingNode("submitter_id", user.getId())) {
                    JsonNode ticketNode = ticketJSONArrayCursor.getCurrentEntityNode();
                    ticketSubjects.add(ticketNode.has("subject") ? ticketNode.get("subject").textValue() : "N/A");
                }
                ticketJSONArrayCursor.start();
                while (ticketJSONArrayCursor.nextMatchingNode("assignee_id", user.getId())) {
                    JsonNode ticketNode = ticketJSONArrayCursor.getCurrentEntityNode();
                    ticketSubjects.add(ticketNode.has("subject") ? ticketNode.get("subject").textValue() : "N/A");
                }

                if (!renderer.enterResult(new UserSearchResultEntry(user, organizationName, ticketSubjects))) {
                    return false;
                }
            }

            organizationJSONArrayCursor.close();
            userJSONArrayCursor.close();
            ticketJSONArrayCursor.close();
        } catch (IOException e) {
            return false;
        }

        if(!renderer.resultsEnd()) {
            return false;
        }
        return true;
    }

    @Override
    public int numberOfResults() {
        return 0;
    }

    @Override
    public String getEntityType() {
        return ApplicationConfiguration.USER;
    }
}
