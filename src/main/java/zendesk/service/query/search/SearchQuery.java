package zendesk.service.query.search;

import zendesk.service.query.Query;

/**
 * Search queries query the source data and renders entity data that matches the given selection criteria.
 */
public interface SearchQuery extends Query {
    /**
     * @return The type of entity data set that this query will be querying.
     */
    String getEntityType();
}
