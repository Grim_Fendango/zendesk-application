package zendesk.service.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.SearchQuery;
import zendesk.service.query.search.json.OrganizationJSONSearchQuery;
import zendesk.service.validation.json.JSONSourceDataValidator;

/**
 * This component checks that the source data is structured correctly. This acts as a facade to abstract away the source
 * data format.
 */
@Component
public class SourceDataValidatorFacade implements SourceDataValidator {

    private final ApplicationConfiguration configuration;

    @Autowired
    public SourceDataValidatorFacade(ApplicationConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * This result of this method is cached to ensure that the process is only run once instance of the applicaiton.
     * In future updates this could be periodically invalidated to perform periodical checks as necessary.
     *
     * @return True if the source entity data files are valid
     */
    @Cacheable(value = "EntityDataValidation")
    public boolean isAllEntityDataValid() {
        if (configuration.isValid()) {
            return getValidator().isAllEntityDataValid();
        }
        return false;
    }

    /**
     * Source Data Validator Factory method.
     *
     * @return A validator implementation that is appropriate for the configured source data format.
     */
    private SourceDataValidator getValidator() {
        if (configuration.isValid()) {
            switch (configuration.getSourceDataFormat()) {
                case ApplicationConfiguration.JSON:
                    return new JSONSourceDataValidator(configuration);
            }
        }
        return null;
    }
}
