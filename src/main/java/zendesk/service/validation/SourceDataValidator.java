package zendesk.service.validation;

/**
 * Checks that the source data is structured correctly.
 */
public interface SourceDataValidator {

    boolean isAllEntityDataValid();
}
