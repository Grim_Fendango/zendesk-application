package zendesk.service.validation.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import zendesk.models.data.Organization;
import zendesk.models.data.SourceDataEntity;
import zendesk.models.data.Ticket;
import zendesk.models.data.User;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.validation.SourceDataValidator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * This component checks that the source data is structured correctly where the source data is in JSON format.
 * This is achieved by attempting to deserialize every element in every root array of the source json files.
 */
public class JSONSourceDataValidator implements SourceDataValidator {

    private final ApplicationConfiguration configuration;

    public JSONSourceDataValidator(ApplicationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public boolean isAllEntityDataValid() {
        return configuration.isValid()
                && isSourceDataValid(configuration.getOrganizationDataPath(), Organization.class)
                && isSourceDataValid(configuration.getUserDataPath(), User.class)
                && isSourceDataValid(configuration.getTicketDataPath(), Ticket.class);
    }

    private boolean isSourceDataValid(String filePath, Class type) {
        try {
            JsonParser jsonParser = (new MappingJsonFactory()).createJsonParser(new File(filePath));
            JsonToken currentToken = jsonParser.nextToken();
            if (currentToken != JsonToken.START_ARRAY) {
                return false; // JSON should be an array
            }
            ObjectMapper mapper = new ObjectMapper();
            while(jsonParser.nextToken() != JsonToken.END_ARRAY) {
                JsonNode entityNode = jsonParser.readValueAsTree();
                Object entity = mapper.treeToValue(entityNode, type);
            }
            return true;
        } catch (IOException e) {
            // NOP
        }
        return false;
    }
}
