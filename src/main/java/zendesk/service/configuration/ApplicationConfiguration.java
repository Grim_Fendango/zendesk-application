package zendesk.service.configuration;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FilenameUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

/**
 *  This component provides access to the source data and results output data configuration information,
 *  i.e., the paths for each data data file, and what type of results output format is being used.
 */
@Component
public class ApplicationConfiguration {
    public static final String JSON = "JSON";
    private static final List<String> SUPPORTED_DATA_FORMATS = ImmutableList.of(JSON);

    public static final String SHELL = "SHELL";
    private static final List<String> SUPPORTED_OUTPUT_FORMAT = ImmutableList.of(SHELL);

    public static final String ORGANIZATION = "ORGANIZATION";
    public static final String USER = "USER";
    public static final String TICKET = "TICKET";
    public static final List<String> SUPPORTED_ENTITY_TYPES = ImmutableList.of(ORGANIZATION, USER, TICKET);

    // FIXME: Get the locations of the data files from an external config file
    private static final String ORGANIZATION_DATA_PATH = "data/organizations.json";

    private static final String USER_DATA_PATH = "data/users.json";

    private static final String TICKET_DATA_PATH = "data/tickets.json";

    private static final String WORKING_DIR = Paths.get("").toAbsolutePath().toString();

    private static final String SOURCE_DATA_FORMAT = JSON;

    private static final String RESULTS_OUTPUT_FORMAT = SHELL;

    private static final int MAX_JOINED_RESULTS = 10;

    /**
     * @return Absolute path to the organisation data file.
     */
    public String getOrganizationDataPath() {
        return WORKING_DIR + "/" + ORGANIZATION_DATA_PATH;
    }

    /**
     * @return Absolute path to the user data file.
     */
    public String getUserDataPath() {
        return WORKING_DIR + "/" + USER_DATA_PATH;
    }

    /**
     * @return Absolute path to the ticket data file.
     */
    public String getTicketDataPath() {
        return WORKING_DIR + "/" + TICKET_DATA_PATH;
    }

    /**
     * @return The configured format of the source data. It is JSON by default.
     */
    public String getSourceDataFormat() {
        return SOURCE_DATA_FORMAT;
    }

    /**
     * @return The configured results output format. It is SHELL by default.
     */
    public String getResultsOutputFormat() {
        return RESULTS_OUTPUT_FORMAT;
    }

    /**
     * @return List of the supported entity types
     */
    public List<String> getSupportedEntityTypes() {
        return SUPPORTED_ENTITY_TYPES;
    }

    /**
     * @return The maximum amount of joined results (information selected from other entities) per search
     * query result. A sensible value should be chosen based on the amount of ram available on the host system
     * as setting this value too high could consume a lot of memory.
     */
    public int getMaxJoinedResults() {
        return MAX_JOINED_RESULTS;
    }

    /**
     * This result of this method is cached to ensure that the process is only run once instance of the applicaiton.
     * In future updates this could be periodically invalidated to perform periodical checks as necessary.
     *
     * @return True: if source entity data files can be found on their configured paths
     */
    @Cacheable(value = "ConfigurationValidation")
    public boolean isValid() {
        if (isSupportedSourceDataFormat() && isSupportedResultsOutputFormat()) {
            return sourcesExistsInConfiguredFormat();
        }
        return false;
    }

    private boolean isSupportedSourceDataFormat() {
        return SUPPORTED_DATA_FORMATS.contains(getSourceDataFormat());
    }

    private boolean sourcesExistsInConfiguredFormat() {
        String sourceDataFormatExtension = getSourceDataFormat().toLowerCase();
        for (String path : getSourceDataPaths()) {
            File file = new File(path);
            if (!file.canRead()) {
                return false;
            }
            if(!FilenameUtils.getExtension(file.getName()).equals(sourceDataFormatExtension)) {
                return false;
            }
        }
        return true;
    }

    private List<String> getSourceDataPaths() {
        return ImmutableList.of(getOrganizationDataPath(), getUserDataPath(), getTicketDataPath());
    }


    private boolean isSupportedResultsOutputFormat() {
        return SUPPORTED_OUTPUT_FORMAT.contains(getResultsOutputFormat());
    }
}
