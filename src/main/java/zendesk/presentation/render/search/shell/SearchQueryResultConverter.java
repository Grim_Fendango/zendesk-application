package zendesk.presentation.render.search.shell;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jibx.schema.codegen.extend.DefaultNameConverter;
import zendesk.models.service.query.search.SearchQueryResultEntry;

import java.util.List;
import java.util.Map;

/**
 * Converts search query result into a 2d array which can be easily rendered by spring shell tabless.
 */
public class SearchQueryResultConverter {

    private static final DefaultNameConverter nameConverter = new DefaultNameConverter();

    /**
     * @param result The result to convert.
     * @param valuesToFlatten List field values that will be flattened. The value in the left column will be a
     *                        depluralized version of the orginal field name with a number prefixed to denote ordering.
     * @return 2d array where the field names are in the left collumn and the corresponding field values are in the
     * right collumn.
     */
    public String[][] convertTo2dArray(SearchQueryResultEntry result, List<String> valuesToFlatten) {
        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        Map<String,Object> resultMap = objectMapper.convertValue(result, Map.class);
        resultMap = flattenListValues(resultMap, valuesToFlatten);
        return convertMapTo2dArray(resultMap);
    }

    private static Map<String,Object> flattenListValues(Map<String,Object> map, List<String> keys) {
        keys.forEach(key -> {
            if(map.containsKey(key)) {
                Object value = map.get(key);
                if (value instanceof List) {
                    map.remove(key);
                    List<Object> valueList = ((List) value);
                    String depluralizedKeyName = nameConverter.depluralize(key);
                    for (int i = 0; i < valueList.size(); i++) {
                        map.put(String.format("%s_%d", depluralizedKeyName, i), valueList.get(i));
                    }
                }
            }
        });
        return map;
    }

    private String[][] convertMapTo2dArray(Map<String,Object> resultEntry) {
        String[][] data = new String[resultEntry.size()][];
        int i = 0;
        for(Map.Entry<String, Object> entry : resultEntry.entrySet()){
            data[i++] = new String[] { entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : ""};
        }
        return data;
    }
}
