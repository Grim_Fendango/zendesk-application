package zendesk.presentation.render.search.shell;

import com.google.common.collect.ImmutableList;
import org.springframework.shell.table.ArrayTableModel;
import org.springframework.shell.table.Table;
import org.springframework.shell.table.TableModel;
import zendesk.models.service.query.search.OrganizationSearchResultEntry;
import zendesk.models.service.query.search.SearchQueryResultEntry;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.presentation.render.styles.shell.ShellQueryResultsRendererStyles;
import zendesk.service.configuration.ApplicationConfiguration;

import java.util.List;

import static zendesk.presentation.render.styles.shell.ShellQueryResultsRendererStyles.printLineSeperator;
import static zendesk.presentation.render.styles.shell.ShellQueryResultsRendererStyles.printNoResultsFound;

/**
 * Renders the results of search queries in the shell that are of he Organization entity type.
 * This only renders one query result at a time to prevent storing the entire result set in memory.
 */
public class ShellOrganizationSearchQueryResultsRenderer implements SearchQueryResultsRenderer {

    private final ApplicationConfiguration configuration;
    private final SearchQueryResultConverter resultConverter = new SearchQueryResultConverter();

    private static final List<String> LIST_PROPERTIES_TO_FLATTEN = ImmutableList.of("tickets", "users");
    private int resultCount = 0;

    public ShellOrganizationSearchQueryResultsRenderer(ApplicationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public boolean resultsStart() {
        System.out.println(ShellQueryResultsRendererStyles.getResultsStartMessage(getEntityType()));
        resultCount = 0;
        return true;
    }

    @Override
    public boolean enterResult(SearchQueryResultEntry result) {
        if (result instanceof OrganizationSearchResultEntry ) {
            resultCount ++;
            printLineSeperator(System.out);
            TableModel model = new ArrayTableModel(resultConverter.convertTo2dArray(result, LIST_PROPERTIES_TO_FLATTEN));
            Table table = ShellQueryResultsRendererStyles.applyStyle(model).build();
            System.out.print(table.render(ShellQueryResultsRendererStyles.MAX_TABLE_RENDER_WIDTH));
            showPaginationMessages((OrganizationSearchResultEntry)result, configuration.getMaxJoinedResults());
            return true;
        }
        return false;
    }

    private void showPaginationMessages(OrganizationSearchResultEntry result, int maxJoinedListValuesToShow) {
        if (result.getTickets().size() >= maxJoinedListValuesToShow) {
            System.out.println(ShellQueryResultsRendererStyles.getPaginationMessage(
                            "organization", "ticket", "organization_id", result.getId()));
        }
        if (result.getUsers().size() >= maxJoinedListValuesToShow) {
            System.out.println(ShellQueryResultsRendererStyles.getPaginationMessage(
                            "organization", "user", "organization_id", result.getId()));
        }
    }

    @Override
    public boolean resultsEnd() {
        if (resultCount > 0) {
            printLineSeperator(System.out);
        } else {
            printNoResultsFound(System.out);
        }
        return true;
    }


    @Override
    public String getEntityType() {
        return ApplicationConfiguration.ORGANIZATION;
    }

    @Override
    public String getResultsOutputFormat() {
        return ApplicationConfiguration.SHELL;
    }
}
