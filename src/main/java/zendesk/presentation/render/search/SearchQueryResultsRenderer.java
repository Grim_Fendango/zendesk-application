package zendesk.presentation.render.search;

import zendesk.models.service.query.search.SearchQueryResultEntry;

/**
 * Renders the results of search queries.
 */
public interface SearchQueryResultsRenderer {
    /**
     * Performs rendering operations that begin a new results set.
     * @return True: if rendering succeded.
     */
    boolean resultsStart();

    /**
     * @param result A search query result to render within the current results set.
     * @return True: if rendering succeded and False: if rendering failed.
     */
    boolean enterResult(SearchQueryResultEntry result);

    /**
     * Performs rendering operations that end a results set.
     * @return True: if rendering succeded
     */
    boolean resultsEnd();

    /**
     * @return The name of the entity type that this renderer is designed to render.
     */
    String getEntityType();

    /**
     * @return The name of the output data format that this render renders to.
     */
    String getResultsOutputFormat();
}
