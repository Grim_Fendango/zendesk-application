package zendesk.presentation.render.search;

import org.springframework.stereotype.Component;
import zendesk.presentation.render.search.shell.ShellOrganizationSearchQueryResultsRenderer;
import zendesk.presentation.render.search.shell.ShellTicketSearchQueryResultsRenderer;
import zendesk.presentation.render.search.shell.ShellUserSearchQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;

/**
 * Used to get search query result renderers specific for the entity type being rendered and the configured
 * results output format.
 */
@Component
public class SearchQueryResultsRendererFactory {

    /**
     * @param entityType The entity type to query. This must be a valid entity type.
     * @param configuration The application configuration, this must be valid.
     * @param renderer The results renderer to use, if null the results will not be rendered.
     * @return SearchQueryResultsRenderer that is appropriate for the configured source data format and entity type
     *         being queried. Null if configuration is invalid or a suitable search query renderer could not be found.
     */
    public SearchQueryResultsRenderer getRenderer(String entityType, ApplicationConfiguration configuration) {

        if (configuration.isValid()) {
            if(configuration.getResultsOutputFormat().equals(ApplicationConfiguration.SHELL)) {
                    switch (entityType) {
                        case ApplicationConfiguration.ORGANIZATION:
                            return new ShellOrganizationSearchQueryResultsRenderer(configuration);
                        case ApplicationConfiguration.USER:
                            return new ShellUserSearchQueryResultsRenderer(configuration);
                        case ApplicationConfiguration.TICKET:
                            return new ShellTicketSearchQueryResultsRenderer(configuration);
                    }
            }
        }

        return null;
    }
}
