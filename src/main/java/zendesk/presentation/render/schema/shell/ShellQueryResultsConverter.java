package zendesk.presentation.render.schema.shell;

import java.util.List;

/**
 * Converts schema query result into a 2d arrays which can be easily rendered by spring shell tabless.
 */
public class ShellQueryResultsConverter {

    /**
     * @return A single column 2d array where the given heading is in the first row, followed by the rest of the data.
     */
    public String[][] convertTo2dArrayWithHeading(List<String> data, String heading) {
        String[][] result = new String [data.size() + 1][1];
        result[0][0] = heading;
        for(int i = 0; i < data.size(); i++) {
            result[i + 1][0] = data.get(i);
        }
        return result;
    }

    /**
     * @return A single column 2d array with one only, where the data is in the first column.
     */
    public String[][] convertSearchQueryTo2dArray(List<String> data) {
        String[][] result = new String [data.size()][1];
        for(int i = 0; i < data.size(); i++) {
            result[i][0] = data.get(i);
        }
        return result;
    }


}
