package zendesk.presentation.render.schema.shell;

import org.springframework.shell.table.ArrayTableModel;
import org.springframework.shell.table.Table;
import org.springframework.shell.table.TableModel;
import zendesk.models.service.query.schema.SchemaQueryResult;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;
import zendesk.presentation.render.styles.shell.ShellQueryResultsRendererStyles;

import java.util.List;

import static zendesk.presentation.render.styles.shell.ShellQueryResultsRendererStyles.printLineSeperator;

/**
 * Renders source data entity decriptions and searchable entitiy lists in the shell.
 */
public class ShellSchemaQueryResultsRenderer implements SchemaQueryResultsRenderer {

    private static final ShellQueryResultsConverter converter = new ShellQueryResultsConverter();

    @Override
    public boolean renderResult(SchemaQueryResult schemaQueryResult) {
        printLineSeperator(System.out);
        System.out.print(getTableWithHeading(schemaQueryResult).render(ShellQueryResultsRendererStyles.MAX_TABLE_RENDER_WIDTH));
        printLineSeperator(System.out);
        return true;
    }

    @Override
    public boolean renderResults(List<SchemaQueryResult> schemaQueryResults) {
        schemaQueryResults.forEach(result -> {
            printLineSeperator(System.out);
            System.out.print(getTableWithHeading(result).render(ShellQueryResultsRendererStyles.MAX_TABLE_RENDER_WIDTH));
        });
        printLineSeperator(System.out);
        return true;
    }

    @Override
    public boolean renderEntityTypesList(List<String> entityTypes) {
        TableModel model = new ArrayTableModel(converter.convertSearchQueryTo2dArray(entityTypes));
        Table table = ShellQueryResultsRendererStyles.applyStyle(model).build();
        printLineSeperator(System.out);
        System.out.print(table.render(ShellQueryResultsRendererStyles.MAX_TABLE_RENDER_WIDTH));
        printLineSeperator(System.out);
        return true;
    }

    private Table getTableWithHeading(SchemaQueryResult schemaQueryResult) {
        TableModel model = new ArrayTableModel(
                converter.convertTo2dArrayWithHeading(schemaQueryResult.getFields(),
                        schemaQueryResult.getEntityType()));
        return ShellQueryResultsRendererStyles.applyStyleWithHeading(model).build();
    }
}
