package zendesk.presentation.render.schema;

import zendesk.models.service.query.schema.SchemaQueryResult;

import java.util.List;

/**
 * Renders descriptions of source data entities and lists of available entities to search.
 */
public interface SchemaQueryResultsRenderer {
    /**
     * Renders a single schema query result
     * @return True: if rendering succeded.
     */
    boolean renderResult(SchemaQueryResult schemaQueryResult);

    /**
     * Renders multiple schema query results
     * @return True: if rendering succeded.
     */
    boolean renderResults(List<SchemaQueryResult> schemaQueryResults);

    /**
     * Renders an entity type list
     * @return True: if rendering succeded.
     */
    boolean renderEntityTypesList(List<String> entityTypes);
}
