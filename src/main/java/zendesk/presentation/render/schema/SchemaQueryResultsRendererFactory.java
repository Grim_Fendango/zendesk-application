package zendesk.presentation.render.schema;

import org.springframework.stereotype.Component;
import zendesk.presentation.render.schema.shell.ShellSchemaQueryResultsRenderer;
import zendesk.service.configuration.ApplicationConfiguration;

/**
 * Used to get schema query result renderers specific for the configured results output format.
 */
@Component
public class SchemaQueryResultsRendererFactory {
    /**
     * @param configuration The application configuration, this must be valid.
     * @param renderer The results renderer to use, if null the results will not be rendered.
     * @return SchemaQueryResultsRenderer that is appropriate for the configured source data format.
     *         Null if configuration is invalid or a suitable schema query renderer could not be found.
     */
    public SchemaQueryResultsRenderer getRenderer(ApplicationConfiguration configuration) {

        if (configuration.isValid()) {
            switch (configuration.getResultsOutputFormat()) {
                case ApplicationConfiguration.SHELL:
                    return new ShellSchemaQueryResultsRenderer();
            }
        }

        return null;
    }
}
