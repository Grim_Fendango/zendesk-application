package zendesk.presentation.render.styles.shell;

import org.jibx.schema.codegen.extend.DefaultNameConverter;
import org.springframework.shell.table.AbsoluteWidthSizeConstraints;
import org.springframework.shell.table.BorderSpecification;
import org.springframework.shell.table.BorderStyle;
import org.springframework.shell.table.CellMatchers;
import org.springframework.shell.table.SimpleHorizontalAligner;
import org.springframework.shell.table.SimpleVerticalAligner;
import org.springframework.shell.table.TableBuilder;
import org.springframework.shell.table.TableModel;
import org.springframework.shell.table.Tables;

import java.io.PrintStream;

/**
 * Some style methods for search query result rendering.
 */
public class ShellQueryResultsRendererStyles {

    public static final String LINE_SEPERATOR = "\t\t\t\t------- xxxx -------";
    public static final String NO_RESULTS_FOUND = "No results found";
    public static final int MAX_TABLE_RENDER_WIDTH = 200;

    public static String getResultsStartMessage(String entityType) {
        return String.format("Search results for %s type:", entityType);
    }

    public static void printNoResultsFound(PrintStream outputStream) {
        outputStream.println(NO_RESULTS_FOUND);
    }

    public static void printLineSeperator(PrintStream outputStream) {
        outputStream.println(ShellQueryResultsRendererStyles.LINE_SEPERATOR);
    }

    public static String getPaginationMessage(String currentEntityType, String entityToSearch, String fieldsToSearch,
            String valueToSearch) {
        DefaultNameConverter nameConverter = new DefaultNameConverter();
        String entityToSearchAsLower = entityToSearch.toLowerCase();
        return String.format("HINT: To view the rest of this %s\'s %s use search %s --field %s --value %s",
                currentEntityType.toLowerCase(), nameConverter.pluralize(entityToSearchAsLower), entityToSearchAsLower,
                fieldsToSearch.toLowerCase(), valueToSearch.toLowerCase());
    }

    public static TableBuilder applyStyle(TableModel tableModel) {
        TableBuilder builder = new TableBuilder(tableModel);
        AbsoluteWidthSizeConstraints sizeConstraints = new AbsoluteWidthSizeConstraints(50);

        builder.addOutlineBorder(BorderStyle.air)
                .paintBorder(BorderStyle.air, BorderSpecification.INNER_VERTICAL)
                .fromTopLeft().toBottomRight()
                .paintBorder(BorderStyle.air, BorderSpecification.INNER_VERTICAL)
                .fromTopLeft().toBottomRight()
                .on(CellMatchers.table())
                .addSizer(sizeConstraints);
        return Tables.configureKeyValueRendering(builder, " = ");
    }

    public static TableBuilder applyStyleWithHeading(TableModel tableModel) {
        TableBuilder builder = applyStyle(tableModel);
        builder.on(CellMatchers.row(0))
                .addAligner(SimpleVerticalAligner.middle)
                .addAligner(SimpleHorizontalAligner.center);
        return builder;
    }
}
