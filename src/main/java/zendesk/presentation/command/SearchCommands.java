package zendesk.presentation.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import zendesk.presentation.render.search.SearchQueryResultsRenderer;
import zendesk.presentation.render.search.SearchQueryResultsRendererFactory;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.search.SearchQuery;
import zendesk.service.query.search.SearchQueryFactory;
import zendesk.service.validation.SourceDataValidator;

/**
 * Search commands for the zendesk application.
 *
 * Search commands are used to search the source entity data and display records of data that meets entered the matching
 * criteria.
 */
@Component
public class SearchCommands implements CommandMarker {

    private static final String SEARCH = "search";
    private static final String FIND_ENTITY_HELP = "the entity to search for matches."
            + "\nHINT: use view entities to view what entities are available.";
    private static final String FIND_FIELD_HELP = "the field (or search term) of the type to search for value matches."
            + "\nHINT: use view fields to see what fields are available";
    private static final String FIND_VALUE_HELP = "the value to match";
    private static final String INVALID_CONFIGURATION = "Source entity data configuration is not valid. Are the files in the "
            + "specified directory?";

    private static final String INVALID_SOURCE_DATA = "Specified source entity data files are invalid. They may not be "
            + "structured correctly or in the correct data format.";
    private static final String INVALID_ENTITY_TYPE = "The entity name given does not match any entities available for searching.";
    private static final String INVALID_FIELD = "The entity does not have a field with that name"
            + "\n HINT: use view <entity> fields to see what fields are available for a specific entity.";

    private final ApplicationConfiguration configuration;
    private final SourceDataValidator dataValidator;
    private final SearchQueryFactory searchQueryFactory;
    private final CommandOptionsValidator optionsValidator;
    private final SearchQueryResultsRendererFactory resultsRendererFactory;

    @Autowired
    public SearchCommands(ApplicationConfiguration configuration, SourceDataValidator dataValidator,
            SearchQueryFactory searchQueryFactory, SearchQueryResultsRendererFactory resultsRendererFactory,
            CommandOptionsValidator optionsValidator) {
        this.configuration = configuration;
        this.dataValidator = dataValidator;
        this.searchQueryFactory = searchQueryFactory;
        this.resultsRendererFactory = resultsRendererFactory;
        this.optionsValidator = optionsValidator;
    }

    /**
     * @return True: if command was successful, False: if command failed or the parameters were invalid.
     */
    @CliCommand(value = SEARCH, help = "Queries the source entity data based on the matching criteria")
    public boolean search(@CliOption(key = {"", "e", "entity"}, help = FIND_ENTITY_HELP, mandatory = true) String entityType,
            @CliOption(key = {"f", "field"}, help = FIND_FIELD_HELP, mandatory = true) String field,
            @CliOption(key = {"v", "value"}, help = FIND_VALUE_HELP, mandatory = true) String valueToMatch
    ) {
        if (!configuration.isValid()) {
            System.out.println(INVALID_CONFIGURATION);
            return false;
        }
        if (!dataValidator.isAllEntityDataValid()) {
            System.out.println(INVALID_SOURCE_DATA);
            return false;
        }
        if (!optionsValidator.isValidEntityTypeParam(entityType)) {
            System.out.println(INVALID_ENTITY_TYPE);
            return false;
        }
        if (!optionsValidator.isValidEntityFieldParam(entityType, field)) {
            System.out.println(INVALID_FIELD);
            return false;
        }
        String sanitizedEntityType = entityType.toUpperCase();
        SearchQueryResultsRenderer renderer = resultsRendererFactory.getRenderer(sanitizedEntityType, configuration);
        SearchQuery query = searchQueryFactory.getQuery(sanitizedEntityType, field, valueToMatch, configuration,
                renderer);
        if (!query.run()) {
            System.out.println("Search query failed");
            return false;
        }

        return true;
    }
}
