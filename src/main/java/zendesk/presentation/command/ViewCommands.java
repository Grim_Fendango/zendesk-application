package zendesk.presentation.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import zendesk.presentation.render.schema.SchemaQueryResultsRenderer;
import zendesk.presentation.render.schema.SchemaQueryResultsRendererFactory;
import zendesk.service.query.schema.SchemaQuery;
import zendesk.service.query.schema.SchemaQueryFactory;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.validation.SourceDataValidator;

/**
 * View commands for the Zendesk application.
 *
 * View commands are used to find what data types and searchable fields are available.
 */
@Component
public class ViewCommands implements CommandMarker{
    private static final String VIEW_ENTITIES = "view entities";
    private static final String VIEW_FIELDS = "view fields";
    private static final String VIEW_FIELDS_ENTITY_HELP = "the entity to display searchable fields for";
    private static final String INVALID_CONFIGURATION = "Source entity data configuration is not valid. Are the files in the "
            + "specified directory?";

    private static final String INVALID_SOURCE_DATA = "Specified source entity data files are invalid. They may not be "
            + "structured correctly or in the correct data format.";
    private static final String INVALID_ENTITY_TYPE = "The entity name given does not match any entities available for searching.";

    private final ApplicationConfiguration configuration;
    private final SourceDataValidator dataValidator;
    private final SchemaQueryFactory schemaQueryFactory;
    private final SchemaQueryResultsRendererFactory resultsRendererFactory;
    private final CommandOptionsValidator optionsValidator;

    @Autowired
    public ViewCommands(ApplicationConfiguration configuration, SourceDataValidator dataValidator,
            SchemaQueryFactory schemaQueryFactory, SchemaQueryResultsRendererFactory resultsRendererFactory,
            CommandOptionsValidator optionsValidator) {
        this.configuration = configuration;
        this.dataValidator = dataValidator;
        this.schemaQueryFactory = schemaQueryFactory;
        this.resultsRendererFactory = resultsRendererFactory;
        this.optionsValidator = optionsValidator;
    }

    /**
     * @return True: if command was successful, False: if command failed or the parameters were invalid.
     */
    @CliCommand(value = VIEW_ENTITIES, help = "Display what entities are available to search.")
    public boolean viewEntities() {
        if (!configuration.isValid()) {
            System.out.println(INVALID_CONFIGURATION);
            return false;
        }
        if (!dataValidator.isAllEntityDataValid()) {
            System.out.println(INVALID_SOURCE_DATA);
            return false;
        }
        SchemaQueryResultsRenderer renderer = resultsRendererFactory.getRenderer(configuration);
        renderer.renderEntityTypesList(configuration.getSupportedEntityTypes());
        return true;
    }

    /**
     * @return True: if command was successful, False: if command failed or the parameters were invalid.
     */
    @CliCommand(value = VIEW_FIELDS, help = "Display searchable fields for a entity")
    public boolean viewFields(
            @CliOption(key = {"", "entity"}, help = VIEW_FIELDS_ENTITY_HELP) String entityType) {
        if (!configuration.isValid()) {
            System.out.println(INVALID_CONFIGURATION);
            return false;
        }
        if (!dataValidator.isAllEntityDataValid()) {
            System.out.println(INVALID_SOURCE_DATA);
            return false;
        }
        if (entityType != null && !optionsValidator.isValidEntityTypeParam(entityType)) {
            System.out.println(INVALID_ENTITY_TYPE);
            return false;
        }
        SchemaQueryResultsRenderer renderer = resultsRendererFactory.getRenderer(configuration);
        if (entityType != null) {
            String sanitizedEntityType = entityType.toUpperCase();
            SchemaQuery query = schemaQueryFactory.getQuery(sanitizedEntityType, configuration, renderer);
            if(!query.run()) {
                System.out.println("Schema query failed");
                return false;
            }
        } else {
            SchemaQuery query = schemaQueryFactory.getQueryForAllEntities(configuration, renderer);
            if(!query.run()) {
                System.out.println("Schema query failed");
                return false;
            }
        }
        return true;
    }
}
