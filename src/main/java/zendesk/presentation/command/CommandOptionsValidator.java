package zendesk.presentation.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import zendesk.service.configuration.ApplicationConfiguration;
import zendesk.service.query.schema.SchemaQuery;
import zendesk.service.query.schema.SchemaQueryFactory;

/**
 * Validates the field and entity type options (parameters) of commands.
 */
@Component
public class CommandOptionsValidator {

    private final SchemaQueryFactory schemaQueryFactory;
    private final ApplicationConfiguration configuration;

    @Autowired
    CommandOptionsValidator(SchemaQueryFactory schemaQueryFactory, ApplicationConfiguration configuration) {
        this.schemaQueryFactory = schemaQueryFactory;
        this.configuration = configuration;
    }

    /**
     * @return True: if entity type is a valid source data entity.
     */
    public boolean isValidEntityTypeParam(String entityType) {
        return entityType != null ?
                configuration.getSupportedEntityTypes().contains(entityType.toUpperCase()) : false;
    }

    /**
     * This result of this method is cached to ensure that the process is only run once for each unique parameter given
     * per instance of the applicaiton.
     * In future updates this could be periodically invalidated to perform periodical checks as necessary.
     * @return True: if field is exists within the entity
     */
    @Cacheable(value = "CommandEntityFieldParamValidation")
    public boolean isValidEntityFieldParam(String entityType, String field) {
        if (isValidEntityTypeParam(entityType)) {
            SchemaQuery query = schemaQueryFactory.getQuery(entityType, configuration, null);
            query.run();
            return query.getResults().size() == 1 ? query.getResults().get(0).getFields().contains(field) : false;
        }
        return false;
    }
}
