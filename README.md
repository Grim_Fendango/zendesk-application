#Zendesk Coding Challenge
Author: Zakariah Chitty

##Purpose
This CLI tool queries a set of json files containing organization, ticket and user information based on user input.

##Assumptions
* The input data must be well formed JSON that contains appropriate fields. In the event that either of these criteria is not met, the program will display a helpful error message.
* This application has been designed with extensibility in mind. The adding of new model types, changing the result output format, or the source data format should be a fairly straightforward task.

##Constraints
* Out of curiosity I decided to not use class based inheritance throughout the project. Common interfaces was considered fine however.

##Project Requirements
This tool requires that Java 8 is installed.

##Getting Started
1. Clone the repo
2. Navigate to the root directory of the repo
3. Execute the following command from the terminal
	
	```
	./gradlew installDist
	```
	
4. Run the following:

	```
	./build/install/coding-challenge/bin/coding-challenge
	```

##Running Tests
###Integration tests
To run the integration tests run the following:

```
./gradlew integrationTest
``` 

###Unit tests
To run the unit tests run the following:

```
./gradlew unitTest
``` 

###All tests
To run all tests run the following:

```
./gradlew test
```

##Usage Guide
###Help
To get help simply enter the following into the shell:

```
help
```

###View fields
To list the available fields for a given entity type enter one of the following:

```
view fields organization
view fields user
view fields ticket
```

Alternatively you can do the following to view all fields for all entity types:

```
view fields
```

###View Available Entities
To list the available entities to search enter the following:

```
view entities
```

###Search
To search the source data for a specific value of a given entity type enter the following:

```
search <entity_type> --field <field_to_search> --value <value_to_match>
```

Alternatively as a shorthand you can do:

```
search <entity_type> --f <field_to_search> --v <value_to_match>
```

##FAQs
Q: How come the nice ASCII art banner and the results are being printed out in a strange way?

A: The lines are probably being wrapped, try making the console window larger.

Q: What's with all of theses classes? 

A: Short answer... Because it's written in java.

Long answer...
For some reason I was given a week instead of the usual 5 hours to complete this task. So I treated it as a learning opportunity and used harsher constraints that warranted stricter separation of concerns.

Q: Where do I even begin with all this code?

A: Ok the project is broken up into a service and presentation layers. 

The presentation layer consists of Command objects that accept CLI commands and Result renderer objects which render the results of search and schema based queries. The service layer handles all of the validation and querying logic.

The project uses dependency injection so while there is a main class it is not very helpful. The best place to start is really the command objects as you get to see what happens when a command is received from the command line.

Q: Why is it only showing the first 10 tickets for an organization or user based query?

A: A decision was made to only render the first 10 in order to not risk having gigabytes of joined data stored in memory at a given time whilst also keeping the interface between the query and results rendering processes relatively straightforward. 

Q: Is there are chance that all of the json source data will be in memory at the same time?

A: By design this should never happen as each result is rendered one at a time and then removed from memory in a stream like fashion and as mentioned before, the joined values are limited to 10 (this is configurable) per result render cycle. However, who knows what Java's default garbage collector really does.

Q: Do you think there is a bit too much indirection going on? I mean do you really need four factory classes?

A: Yeah a bare bones solution could have gotten away with a lot less but in order to abstract away the search / data joining strategy, the rendering techniques and the underlying source data type it was necessary.


